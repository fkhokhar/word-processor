#include "machine.h"

#define ROWS 25
#define COLS 80

#define BLACK 0
#define BLUE 1
#define GREEN 2
#define CYAN 3
#define RED 4
#define MAGENTA 5
#define BROWN 6
#define LIGHT_GRAY 7
#define DARK_GRAY 8
#define LIGHT_BLUE 9
#define LIGHT_GREEN 10
#define LIGHT_CYAN 11
#define LIGHT_RED 12
#define LIGHT_MAGENTA 13
#define LIGHT_BROWN 14
#define WHITE 15

void kernelMain(void) {

unsigned char keymap[256] = {
    0,  0, '1', '2', '3', '4', '5', '6', '7', '8',	/* 9 */
  '9', '0', 0, 0, '\b',	/* Backspace */
  0,			/* Tab */
  'q', 'w', 'e', 'r',	/* 19 */
  't', 'y', 'u', 'i', 'o', 'p', 0, 0, '\n',	/* Enter key */
    0,			/* 29   - Control */
  'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 0,	/* 39 */
 0, 0,   '*',		/* Left shift */
 0, 'z', 'x', 'c', 'v', 'b', 'n',			/* 49 */
  'm', 0, 0, 0,   0,				/* Right shift */
  '*',
    0,	/* Alt */
  0,	/* Space bar */
    0,	/* Caps lock */
    0,	/* 59 - F1 key ... > */
    0,   0,   0,   0,   0,   0,   0,   0,
    0,	/* < ... F10 */
    0,	/* 69 - Num lock*/
    0,	/* Scroll Lock */
    0,	/* Home key */
    0,	/* Up Arrow */
    0,	/* Page Up */
  0,
    0,	/* Left Arrow */
    0,
    0,	/* Right Arrow */
  0,
    0,	/* 79 - End key*/
    0,	/* Down Arrow */
    0,	/* Page Down */
    0,	/* Insert Key */
  '\b',	/* Delete Key */
    0,   0,   0,
    0,	/* F11 Key */
    0,	/* F12 Key */
    0,	/* All other keys are undefined */
};
		
short *screen = (short*) 0xb8000;
unsigned int offset = 0;
int shift = 0; //boolean value

//clear screen with blue

	for(int blue = 0; blue < 2000; blue++){
		*(screen + blue) = 0x1100;
	}

//sets cursor to (0,0)
	
	outb(0x3d4,0x0f);
	outb(0x3d5, offset);
	outb(0x3d4, 0x0e);
	outb(0x3d5, offset>>8);

while(1){
	
	unsigned char dataPresent = inb(0x64);

	if(dataPresent % 2 == 1){
	
		unsigned char scancode = inb(0x60);
	
		unsigned char myChar = keymap[scancode];

		dataPresent = dataPresent>>1;

		if(dataPresent % 2 == 1){
			// do nothing 
		} else{
			int counter = 0;
			
			if(myChar == 0){  //case for unmapped keys

				//do nothing
	
			} else if(myChar == '*'){ //shift was pressed
				shift = 1;
			} else if (scancode == 0xAA || scancode == 0xB6){ //shift was released
				shift = 0;
			}else if(offset >= 2000){ //scrolling

				for(int i = 0; i < 25; i++){
					for(int j = 0; j < 80; j++){
					*(screen + (counter * 80) + j) = *(screen + ((counter + 1) * 80) + j);
					}
					counter++;
				}
				
				offset = 1920;
				
				for(int l = 0; l < 80; l++){
				*(screen + offset + l) = 0x1F00;
				}

				*(screen + offset) = 0x1F00 + myChar;
				offset++;

				outb(0x3d4,0x0f);
				outb(0x3d5, offset);
				outb(0x3d4, 0x0e);
				outb(0x3d5, offset>>8);

			} else if(myChar == '\n') {  //case for enter key
				unsigned int value = offset % 80;
				unsigned int remainder  = 80 - value;
				offset += remainder;
				
				outb(0x3d4,0x0f);
				outb(0x3d5, offset);
				outb(0x3d4, 0x0e);
				outb(0x3d5, offset>>8);
			} else if (myChar == '\b'){  //case for backspace/delete
				if(offset % 80 == 0){
					//do nothing, just stay there
				} else {
					offset--;
					*(screen + offset) = 0x1F00;
					outb(0x3d4,0x0f);
					outb(0x3d5, offset);
					outb(0x3d4, 0x0e);
					outb(0x3d5, offset>>8);
				}
			
			} else{ 

				if(shift  == 1){ //shift has been pressed
					myChar = myChar - 32;
					
					*(screen + offset) = 0x1F00 + myChar;
					offset++;

					outb(0x3d4,0x0f);
					outb(0x3d5, offset);
					outb(0x3d4, 0x0e);
					outb(0x3d5, offset>>8);
					shift = 0;
		
				} else if (shift == 0){

				//case for normal condition
			
				*(screen + offset) = 0x1F00 + myChar;
				offset++;

				outb(0x3d4,0x0f);
				outb(0x3d5, offset);
				outb(0x3d4, 0x0e);
				outb(0x3d5, offset>>8);
				}
			}
		}
	}

}	

}

